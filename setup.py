#! /usr/bin/env python
try:  # for pip >= 10
    from pip._internal.req import parse_requirements
except ImportError:  # for pip <= 9.0.3
    from pip.req import parse_requirements

from setuptools import find_packages, setup


def convert_req_format(resource: str):
    egg, uri = resource.split('@')
    return '{uri}#egg={egg_name}'.format(uri=uri, egg_name=egg).strip()


requires = [str(ir.req) for ir in parse_requirements('requirements.txt',
                                                     session='No Session')]

dependency_links = [convert_req_format(req) for req in requires if '@' in req]
tests_require = ['pytest-mock', 'pytest-cov']

setup(name='wcl_parse',
      use_scm_version=True,
      description='A commandline tool used to search for spell casts'
                  ' in a raid uploaded to Warcraft Logs',
      classifiers=['Development Status :: 2 - Pre-Alpha',
                   'Environment :: Console',
                   'Operating System :: OS Independent',
                   'Programming Language :: Python'],
      author='Nick Wilburn',
      author_email='senior.crepe@gmail.com',
      license='MIT License',
      entry_points={
        'console_scripts': ['wcl_parse=wcl_parse.parse:main']
      },
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,
      dependency_links=dependency_links,
      install_requires=requires,
      setup_requires=['setuptools-scm'],
      extras_require={'test': tests_require}
)
