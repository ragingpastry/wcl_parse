import argparse
import requests
import os
import pprint

wcl_url = "https://classic.warcraftlogs.com/v1"


def pull_fights(fight_id, api_key):
    r = requests.get(f"{wcl_url}/report/fights/{fight_id}?api_key={api_key}")

    return r.json()


def parse_friendly(fight_data, which_class="warrior"):
    return [
        player
        for player in fight_data["friendlies"]
        if player["type"].lower() == which_class
    ]


def parse_bosses(fight_data):
    return [boss for boss in fight_data["enemies"] if boss["type"] == "Boss"]


def pull_fight_casts(fight_data, fight_id, boss_id, api_key):
    fight_start_time = fight_data["start_time"]
    fight_end_time = fight_data["end_time"]

    r = requests.get(
        f"{wcl_url}/report/events/casts/{fight_id}"
        f"?start={fight_start_time}&end={fight_end_time}"
        f"&targetid={boss_id}&api_key={api_key}"
    )
    casts = r.json()

    return casts


def parse_fights(
    raid_id, player_class, spell_to_search, num_casts, display_casts, api_key
):
    fights = pull_fights(raid_id, api_key)
    players = parse_friendly(fights, which_class=player_class)

    bosses = parse_bosses(fights)

    fight_data = [fight for fight in fights["fights"] if fight["boss"] != 0]
    for fight in fight_data:
        for boss in bosses:
            boss_id = boss["id"] if boss["name"] == fight["name"] else None
            if boss_id:
                fight_casts = pull_fight_casts(fight, raid_id, boss_id, api_key)
                player_gcds = []
                for player in players:
                    if fight["id"] in [fight["id"] for fight in player["fights"]]:
                        player_casts = [
                            cast
                            for cast in fight_casts["events"]
                            if cast["sourceID"] == player["id"]
                        ]
                        if num_casts == "all":
                            ability_names = [
                                cast["ability"]["name"] for cast in player_casts
                            ]
                        else:
                            ability_names = [
                                cast["ability"]["name"]
                                for cast in player_casts[0 : num_casts - 1]
                            ]

                        player_gcds.append({player["name"]: ability_names})
                print(f"\nBoss: {fight['name']}")
                if display_casts:
                    pprint.pprint(player_gcds)
                for player_gcd in player_gcds:
                    caster = list(player_gcd.keys())[0]
                    spells = list(player_gcd.values())[0]
                    if spell_to_search and spell_to_search.lower() not in [
                        item.lower() for item in spells
                    ]:
                        print(f"{caster} did not cast {spell_to_search}!")


def gen_parser():
    description = """
    Parses Warcraft Logs for the first casts made by every player
    of a class on a boss encounter
    """
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument(
        "--api-key",
        dest="api_key",
        default=os.environ.get("WCL_API_KEY"),
        help="API key used to authenticate to Warcraft Logs",
    )
    parser.add_argument(
        "--raid-id",
        dest="raid_id",
        default=os.environ.get("WCL_RAID_ID"),
        help="The raid ID to parse",
    )
    parser.add_argument(
        "--class",
        dest="player_class",
        default="warrior",
        help="The class to search for",
    )
    parser.add_argument(
        "--spell", dest="spell", default=False, help="The spell to look for"
    )
    parser.add_argument(
        "--num-casts",
        dest="num_casts",
        default=3,
        help="The number of casts to search in",
    )
    parser.add_argument(
        "--no-display-casts", action="store_false", help="Display all casts found"
    )

    return parser.parse_args()


def main():

    args = gen_parser()

    if args.num_casts is not None and args.num_casts.isdigit():
        args.num_casts = int(args.num_casts)
    elif args.num_casts.lower() != "all":
        print("Invalid argument.")

    parse_fights(
        raid_id=args.raid_id,
        player_class=args.player_class.lower(),
        spell_to_search=args.spell,
        num_casts=args.num_casts,
        display_casts=args.no_display_casts,
        api_key=args.api_key,
    )
