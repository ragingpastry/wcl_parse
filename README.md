# wcl_parse

## A tool used to parse Warcraft Logs and look for specific spell casts across a class for an entire raid.

The purpose of this tool is to provide an easy interface to search for spell casts across
a specified number of casts for a class. For example, if you wanted to know which warriors were not 
casting sunder armor in their first cast of a boss fight, or if you wanted to see which warlocks were 
casting Corruption, this would be the tool for you.

This tool could also be used as a part of a Discord bot or automated pipeline. For example, a piece
of automation could be configured to watch for new guild parses to be uploaded to Warcraft Logs. Once
a new parse is found this tool could be kicked off to run on the new raid ID and return the results to a discord channel.

### Installation

wcl_parse requires python 3. There is currently no pip package for this repository.

```shell
git clone https://gitlab.com/ragingpastry/wcl_parse
cd wcl_parse
pip install -r requirements.txt
python setup.py install
```

### Options

```
usage: wcl_parse [-h] [--api-key API_KEY] [--raid-id RAID_ID]
                 [--class PLAYER_CLASS] [--spell SPELL]
                 [--num-casts NUM_CASTS] [--no-display-casts]

Parses Warcraft Logs for the first casts made by every player of a class on a
boss encounter

optional arguments:
  -h, --help            show this help message and exit
  --api-key API_KEY     API key used to authenticate to Warcraft Logs
  --raid-id RAID_ID     The raid ID to parse
  --class PLAYER_CLASS  The class to search for
  --spell SPELL         The spell to look for
  --num-casts NUM_CASTS
                        The number of casts to search in
  --no-display-casts    Display all casts found


```

An environment variable can also be used for the API_KEY.

`export WCL_API_KEY=<my api key>`

### Examples

This example shows a query which will search for the spell cast `Sumder Armor` in the first two casts from the `warrior` class across all bosses. We will also display the spells that were cast.

```shell
wcl_parse --api-key<api_key> --raid-id=txCAkG26jL3PKvnM --class=warrior --spell="Sunder Armor" --num-casts 2
Boss: Golemagg the Incinerator
[{'Jonnytsunami': ['Sunder Armor', 'Bloodthirst']},
 {'Kickproof': ['Taunt', 'Bloodthirst']},
 {'Siax': ['Sunder Armor', 'Sunder Armor']},
 {'Ragingpastry': ['Charge', 'Sunder Armor']},
 {'Schwarbygasm': ['Sunder Armor', 'Sunder Armor']}]
Kickproof did not cast sunder armor!
```

This example shows a query which will search for the spell cast `Corruption` across all warlocks on all bosses.

```shell
wcl_parse --api-key <api-key>--raid-id=txCAkG26jL3PKvnM --class=warlock --spell="Corruption" --num-casts all --no-display-casts
Boss: Lucifron
Ceejay did not cast corruption!

Boss: Magmadar
Ceejay did not cast corruption!

Boss: Gehennas
Ceejay did not cast corruption!
```


